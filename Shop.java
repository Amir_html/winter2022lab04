import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Scanner reader= new Scanner(System.in);
		Song[] mySongList=new Song[4];
		for(int i=0;i<mySongList.length;i++){
			System.out.println("Who is the artist, what is the release date and what is the name of the song?");
			mySongList[i]=new Song(reader.next(),reader.nextInt(),reader.next());
			/*System.out.println("Who is the artist?");
			mySongList[i].setArtist(reader.next());
			System.out.println("When was it released?");
			mySongList[i].setRelease(reader.nextInt());
			System.out.println("What is the name of the song?");
			mySongList[i].setName(reader.next());*/
		}
		//Before changing the value of the last field
		System.out.println("Artist: "+mySongList[mySongList.length-1].getArtist());
		System.out.println("Release date: "+mySongList[mySongList.length-1].getRelease());
		System.out.println("Name of the song: "+mySongList[mySongList.length-1].getName());
		
		System.out.println("Who is the artist?");
		mySongList[mySongList.length-1].setArtist(reader.next());
		System.out.println("When was it released?");
		mySongList[mySongList.length-1].setRelease(reader.nextInt());
		System.out.println("What is the name of the song?");
		mySongList[mySongList.length-1].setName(reader.next());
		
		//After changing the value of the last field
		System.out.println("Artist: "+mySongList[mySongList.length-1].getArtist());
		System.out.println("Release date: "+mySongList[mySongList.length-1].getRelease());
		System.out.println("Name of the song: "+mySongList[mySongList.length-1].getName());
		mySongList[mySongList.length-1].appreciation();
	}
}