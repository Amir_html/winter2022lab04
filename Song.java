public class Song{
	private String artist;
	private int release;
	private String name;
	public Song(String artist,int release,String name){
		this.artist=artist;
		this.release=release;
		this.name=name;
	}
	public void appreciation(){
		System.out.println("I love "+artist+"'s "+name+" that was released on "+release);
	}
	
	public String getArtist(){
		return this.artist;
	}
	public int getRelease(){
		return this.release;
	}
	public String getName(){
		return this.name;
	}
	
	public void setArtist(String artist){
		this.artist=artist;
	}
	public void setRelease(int release){
		this.release=release;
	}
	public void setName(String name){
		this.name=name;
	}
	
}